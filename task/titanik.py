import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    """
    Didn't had time to filter in "Mrs and Miss" and check for missing values
    """
    male_median_age = int((df.loc[df['Sex'] == 'male']['Age']).median())
    female_median_age =int((df.loc[df['Sex'] == 'female']['Age']).median())
    return [('Mr.', male_median_age, 0), ('Mrs', female_median_age, 0)]


if __name__ == '__main__':
    df = get_titatic_dataframe()

    print(get_filled())